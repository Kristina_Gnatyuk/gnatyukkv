package Lab3.TestRozetka;

import Lab3.TestRozetka.RozetkaMainPage;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.util.concurrent.TimeUnit;

public class Tests {
    private static WebDriver driver;
    private static RozetkaMainPage mainPage;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        driver.get("http://rozetka.com.ua/");
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().window().maximize();
        mainPage = new RozetkaMainPage(driver);
    }

    @AfterClass
    public static void TearDown() {
        driver.close();
    }

    @Test
    public void checkLogo() {
       RozetkaMainPage rozetka = new RozetkaMainPage(driver);
       rozetka.checkLogo();
    }

    @Test
    public void checkApple() {
        RozetkaMainPage rozetka = new RozetkaMainPage(driver);
        rozetka.checkApple();
    }

    @Test
    public void checkMp3InMenu() {
        RozetkaMainPage rozetka = new RozetkaMainPage(driver);
        rozetka.checkMP3();
    }

    @Test
    public void checkCities() {
        SelectCityPage page = mainPage.navigateToSelectCityPage();
        page.checkKharkov();
        page.checkKyiv();
        page.checkOdessa();
    }

    @Test
    public void checkEmptyCart() {
        CartPage page = mainPage.navigateToCartPage();
        page.checkCart();
    }
}
