package Lab3.TestStackoverflow;

import Lab3.TestRozetka.RozetkaMainPage;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

public class TestsStack {
    private static WebDriver driver;
    private static StackoverflowMainPage mainPage;

    @BeforeClass
    public static void SetUp() {
        driver = new FirefoxDriver();
        mainPage = new StackoverflowMainPage(driver);
    }

    @Before
    public void SetUpTest(){
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
    }

    @AfterClass
    public static void TearDown() {
        driver.close();
    }

    @Test
    public void checkFeatured(){
        StackoverflowMainPage quantity = new StackoverflowMainPage(driver);
        quantity.checkFeaturedNumber();
    }

    @Test
    public void checkSocialNetworksOnSignUpPage() {
        SignUpPage link = mainPage.navigateToSignUpPage();
        link.checkGoogleLink();
        link.checkFacebookLink();
    }

    @Test
    public void checkDateOnQuestionPage() {
        QuestionPage page = mainPage.navigateToQuestionPage();
        page.checkDate();
    }

    @Test
    public void checkSalaryOnTitle() {
        StackoverflowMainPage salary = new StackoverflowMainPage(driver);
        salary.checkSalary();
    }
}
