package Lab4.steps;

import Lab3.TestRozetka.CartPage;
import Lab3.TestRozetka.RozetkaMainPage;
import Lab3.TestRozetka.SelectCityPage;
import Lab4.runner.TestRun;
import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Lab4.runner.TestRun.driver;
import static org.junit.Assert.assertTrue;


public class TestRozetka {
    private static RozetkaMainPage mainPage;
    private static CartPage cartPage;
    private static SelectCityPage selectCityPage;

    public TestRozetka(){
        mainPage = new RozetkaMainPage(driver);
        cartPage = new CartPage(driver);
        selectCityPage = new SelectCityPage(driver);
    }



    @Given("^I on page Rozetka$")
    public void iOnPageRozetka() throws Throwable {
        if (!driver.getTitle().equals("Интернет-магазин ROZETKA™: фототехника, видеотехника, аудиотехника, компьютеры и компьютерные комплектующие"))
            driver.get("http://rozetka.com.ua/");
    }

    @When("^I click Cart link$")
    public void iClickCartLink() throws Throwable {
        mainPage.navigateToCartPage();
    }

    @Then("^I see Cart is empty$")
    public void iSeeCartIsEmpty() throws Throwable {
        cartPage.checkCart();
    }


    @Then("^I see logo$")
    public void iSeeLogo() throws Throwable {
        RozetkaMainPage rozetka = new RozetkaMainPage(driver);
        rozetka.checkApple();
    }

    @Then("^I see mp(\\d+)$")
    public void iSeeMp(int arg0) throws Throwable {
        RozetkaMainPage rozetka = new RozetkaMainPage(driver);
        rozetka.checkMP3();
    }

    @Then("^I see apple$")
    public void iSeeApple() throws Throwable {
        RozetkaMainPage rozetka = new RozetkaMainPage(driver);
        rozetka.checkApple();
    }

    @When("^I click SelectCity link$")
    public void iClickSelectCityLink() throws Throwable {
        mainPage.navigateToSelectCityPage();
    }

    @Then("^I see cities: Kharkov, Kiev, Odessa$")
    public void iSeeCitiesKharkovKievOdessa() throws Throwable {
        selectCityPage.checkKharkov();
        selectCityPage.checkKyiv();
        selectCityPage.checkOdessa();
    }
}
