package Lab4.steps;

import Lab3.TestStackoverflow.QuestionPage;
import Lab3.TestStackoverflow.SignUpPage;
import Lab3.TestStackoverflow.StackoverflowMainPage;
import Lab4.runner.TestRun;
import cucumber.api.PendingException;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import static Lab4.runner.TestRun.driver;
import static org.junit.Assert.assertTrue;

public class TestStackoverflow {
    private static StackoverflowMainPage stackoverflowPage;
    private static QuestionPage questionPage;
    private static SignUpPage signUpPage;

    public TestStackoverflow() {
        stackoverflowPage = new StackoverflowMainPage(driver);
        questionPage = new QuestionPage(driver);
        signUpPage = new SignUpPage(driver);
    }

    @When("^I on start page Stackoverflow$")
    public void iOnStartPageStackoverflow() throws Throwable {
        if (!driver.getTitle().equals("Stack Overflow"))
            driver.get("http://stackoverflow.com/");
    }

    @Then("^I see ‘featured’ is more than (\\d+)$")
    public void iSeeFeaturedIsMoreThan(int arg0) throws Throwable {
        stackoverflowPage.checkFeaturedNumber();
    }

    @When("^I click Sign Up button$")
    public void iClickSignUpButton() throws Throwable {
        stackoverflowPage.navigateToSignUpPage();
    }

    @Then("^I see buttons Google and Facebook on Sing Up page$")
    public void iSeeButtonsGoogleAndFacebookOnSingUpPage() throws Throwable {
        signUpPage.checkFacebookLink();
        signUpPage.checkGoogleLink();
    }

    @When("^I click question link$")
    public void iClickQuestionLink() throws Throwable {
        stackoverflowPage.navigateToQuestionPage();
    }

    @Then("^I see today date is displayed$")
    public void iSeeTodayDateIsDisplayed() throws Throwable {
        questionPage.checkDate();
    }


    @Then("^I see salary more than (\\d+)k$")
    public void iSeeSalaryMoreThanK(int arg0) throws Throwable {
        stackoverflowPage.checkSalary();
    }
}
