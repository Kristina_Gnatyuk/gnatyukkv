@rozetka
Feature: test Rozetka site

  @logo
  Scenario: check logo on Rozetka start page
    When I on page Rozetka
    Then I see logo

  @apple
  Scenario: check apple on Rozetka start page
    When I on page Rozetka
    Then I see apple

  @mp3
  Scenario: check mp3 on Rozetka start page
    When I on page Rozetka
    Then I see mp3

  @city
  Scenario: check Cities on Rozetka site
    Given I on page Rozetka
    When I click SelectCity link
    Then I see cities: Kharkov, Kiev, Odessa

  @cart
  Scenario: check Cart on Rozetka site
    Given I on page Rozetka
    When I click Cart link
    Then I see Cart is empty



