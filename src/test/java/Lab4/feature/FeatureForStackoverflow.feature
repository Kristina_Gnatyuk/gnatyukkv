@stackoverflow
Feature: test Stackoverflow site

  @featured
  Scenario: check ‘featured’ is more than 300 on Stackoverflow start page
    When I on start page Stackoverflow
    Then I see ‘featured’ is more than 300

  @socialNetworks
  Scenario: check buttons Google and Facebook on Sing Up page
    Given I on start page Stackoverflow
    When I click Sign Up button
    Then I see buttons Google and Facebook on Sing Up page

  @date
   Scenario: check that date is today on Top Question page
     Given I on start page Stackoverflow
     When I click question link
     Then I see today date is displayed

  @salary
  Scenario: check salary more than $ 100k on start page
    Given I on start page Stackoverflow
    Then I see salary more than 100k




