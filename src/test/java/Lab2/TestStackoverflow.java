package Lab2;

import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;

public class TestStackoverflow {
    private WebDriver driver;

    @Before
    public void setUpBefore() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://stackoverflow.com/");
        driver.manage().window().maximize();
    }

    @After
    public void tearDownAfter() throws Exception {
        driver.close();
    }

    @Test
    public void Featured() {
        WebElement featured = driver.findElement(By.xpath(".//span[@class='bounty-indicator-tab']"));
        String quantity = featured.getText();
        assertTrue("Value is less than 300",(Integer.valueOf(quantity) > 300));
    }

    @Test
    public void SignUp() {
        WebElement signup = driver.findElement(By.id("tell-me-more"));
        signup.click();
        WebElement google = driver.findElement(By.xpath(".//*[contains(text(),'Google')]/.."));
        WebElement facebook = driver.findElement(By.xpath(".//*[contains(text(),'Facebook')]/.."));
    }

    @Test
    public void Question() {
        WebElement question = driver.findElement(By.className("question-hyperlink"));
        question.click();
        WebElement date = driver.findElement(By.xpath(".//b[text()='today']"));
    }

    @Test
    public void Salary() {
        String title = driver.findElement(By.xpath(".//*[@class='title']")).getText();
        Pattern pat = Pattern.compile("\\d+");
        Matcher matcher = pat.matcher(title);
        if (matcher.find()) {
            String a = matcher.group();
            int i = Integer.parseInt(a);
            assertTrue("Test failed", i > 100);
        }

    }

}
