package Lab2;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.util.concurrent.TimeUnit;

import static org.junit.Assert.assertNotNull;

public class TestRozetka {
    private WebDriver driver;

    @Before
    public void setUpBefore() throws Exception {
        driver = new FirefoxDriver();
        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        driver.get("http://rozetka.com.ua/");
        driver.manage().window().maximize();
    }

    @After
    public void tearDownAfter() throws Exception {
        driver.close();
    }

    @Test
    public void SearchLogo() {
        WebElement logo = driver.findElement(By.xpath(".//div[@class='logo']"));
        assertNotNull("Logo is absent", logo);
    }

    @Test
    public void ParagraphApple() {
        WebElement apple = driver.findElement(By.xpath(".//*[@menu_id='2195']"));
        assertNotNull("Paragraph Apple is absent", apple);
    }

    @Test
    public void MP3() {
        WebElement mp3 = driver.findElement(By.xpath(".//li[@class='m-main-l-i']/a[contains(text(),'MP3')]"));
        assertNotNull("MP3 is not found", mp3);
    }

    @Test
    public void CityChoose() {
        WebElement city = driver.findElement(By.xpath(".//div[@id='city-chooser']"));
        city.click();
        WebElement Kyiv = driver.findElement(By.xpath(".//*[@locality_id='1']"));
        WebElement Odessa = driver.findElement(By.xpath(".//*[@locality_id='30']"));
        WebElement Kharkov = driver.findElement(By.xpath(".//*[@locality_id='31']"));
    }

    @Test
    public void Cart() {
        WebElement cart = driver.findElement(By.xpath(".//span[text()='Корзина']"));
        cart.click();
        WebElement cartEmpty = driver.findElement(By.xpath(".//h2[@class='empty-cart-title inline sprite-side']"));
    }
}
