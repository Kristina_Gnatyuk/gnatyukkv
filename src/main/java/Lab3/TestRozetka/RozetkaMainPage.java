package Lab3.TestRozetka;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertTrue;

public class RozetkaMainPage {
    private WebDriver driver;

    public RozetkaMainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[@class='logo']")
    public WebElement logo;

    @FindBy(xpath = ".//ul[@id='m-main']/li/a")
    public List<WebElement> menuItemsList;

    @FindBy(xpath = ".//*[@menu_id='2195']")
    public WebElement apple;

    @FindBy(xpath = ".//li[@class='m-main-l-i']/a[contains(text(),'MP3')]")
    public WebElement mp3;

    @FindBy(xpath = ".//div[@id='city-chooser']")
    public WebElement cityLink;

    @FindBy(xpath = ".//span[text()='Корзина']")
    public WebElement cartLink;

//    public List<String> getMenuItemNames(){
//        List<String> menuItemNames = new ArrayList<String>();
//        for(WebElement element : menuItemsList){
//            menuItemNames.add(element.getText());
//        }
//
//        return menuItemNames;
//    }

//    public boolean checkElement() {
//
//    }



    public void checkLogo() {
        assertTrue("Logo isn't displayed",logo.isDisplayed());
    }

    public void checkApple() {
        assertTrue("Paragraph Apple is absent",apple.isDisplayed());
    }

    public void checkMP3() {
        assertTrue("MP3 is not found",mp3.isDisplayed());
    }

    public SelectCityPage navigateToSelectCityPage() {
        cityLink.click();
        return new SelectCityPage(driver);
    }

    public CartPage navigateToCartPage() {
        cartLink.click();
        return new CartPage(driver);
    }
}