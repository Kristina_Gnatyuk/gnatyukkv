package Lab3.TestRozetka;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class SelectCityPage {
    private WebDriver driver;

    public SelectCityPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//div[@id='city-chooser']")
    public WebElement cityLink;

    @FindBy(xpath = ".//*[@locality_id='1']")
    public WebElement Kyiv;

    @FindBy(xpath = ".//*[@locality_id='30']")
    public WebElement Odessa;

    @FindBy(xpath = ".//*[@locality_id='31']")
    public WebElement Kharkov;


    public void checkKyiv() {
        assertTrue("Logo isn't displayed",Kyiv.isDisplayed());
    }

    public void checkOdessa() {
        assertTrue("Logo isn't displayed",Odessa.isDisplayed());
    }

    public void checkKharkov() {
        assertTrue("Logo isn't displayed",Kharkov.isDisplayed());
    }

}
