package Lab3.TestRozetka;

import junit.framework.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class CartPage {
    private WebDriver driver;

    public CartPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//h2[@class='empty-cart-title inline sprite-side']")
    public WebElement cartEmpty;

    public void checkCart() {
        assertTrue("Cart is not empty",cartEmpty.getText().contains("Корзина пуста"));
    }

}
