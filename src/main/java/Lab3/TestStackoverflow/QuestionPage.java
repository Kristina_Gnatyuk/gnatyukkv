package Lab3.TestStackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class QuestionPage {
    private WebDriver driver;

    public QuestionPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//b[text()='today']")
    public WebElement date;

    public void checkDate() {
        assertTrue("Date isn't today",date.isDisplayed());
    }
}
