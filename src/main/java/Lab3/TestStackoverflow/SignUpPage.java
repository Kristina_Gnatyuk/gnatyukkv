package Lab3.TestStackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import static org.junit.Assert.assertTrue;

public class SignUpPage {
    private WebDriver driver;

    public SignUpPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//*[contains(text(),'Google')]/..")
    public WebElement google;

    @FindBy(xpath = ".//*[contains(text(),'Facebook')]/..")
    public WebElement facebook;


    public void checkGoogleLink() {
        assertTrue("Google link isn't displayed",google.isDisplayed());
    }

    public void checkFacebookLink() {
        assertTrue("Facebook link isn't displayed",facebook.isDisplayed());
    }

}
