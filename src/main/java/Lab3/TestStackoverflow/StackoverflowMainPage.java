package Lab3.TestStackoverflow;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static org.junit.Assert.assertTrue;


public class StackoverflowMainPage {
    private WebDriver driver;

    public StackoverflowMainPage(WebDriver driver) {
        PageFactory.initElements(driver, this);
        this.driver = driver;
    }

    @FindBy(xpath = ".//span[@class='bounty-indicator-tab']")
    public WebElement featured;

    @FindBy(id = "tell-me-more")
    public WebElement signUpButton;

    @FindBy(className = "question-hyperlink")
    public WebElement questionLink;

    @FindBy(xpath = ".//*[@class='title']")
    public WebElement title;


    public void checkFeaturedNumber(){
        String quantity = featured.getText();
        assertTrue("Value is less than 300",(Integer.valueOf(quantity) > 300));
    }

    public SignUpPage navigateToSignUpPage() {
        signUpButton.click();
        return new SignUpPage(driver);
    }

    public QuestionPage navigateToQuestionPage() {
        questionLink.click();
        return new QuestionPage(driver);
    }

    public void checkSalary() {
        Pattern pat = Pattern.compile("\\d+");
        Matcher matcher = pat.matcher(title.getText());
        if (matcher.find()) {
            String a = matcher.group();
            int i = Integer.parseInt(a);
            assertTrue("Test failed", i > 100);
        }
    }
}
